
-- -----------------------------------------------------
-- Table `projectdb`.`user_seq`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectdb`.`user_seq` (
    `next_val` BIGINT NULL DEFAULT NULL)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;

USE `projectdb` ;

-- -----------------------------------------------------
-- procedure delete_category
-- -----------------------------------------------------

DELIMITER $$
USE `projectdb`$$
CREATE DEFINER=`sgpd_admin`@`localhost` PROCEDURE `delete_category`(IN p_category_id INT)
BEGIN
UPDATE category SET category.deleted = 1 WHERE id = p_category_id;
UPDATE task SET task.deleted = 1 WHERE category_id = p_category_id;
end$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure delete_task
-- -----------------------------------------------------

DELIMITER $$
USE `projectdb`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_task`(IN task_id INT)
BEGIN
UPDATE task SET task.deleted = TRUE WHERE id = task_id;
end$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure get_all_task_by_category_id
-- -----------------------------------------------------

DELIMITER $$
USE `projectdb`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_task_by_category_id`(IN p_category_id int)
BEGIN
SELECT task.*, c.color AS categoryColor, c.name AS categoryName
FROM task
         INNER JOIN category c ON task.category_id = c.id
WHERE task.category_id = p_category_id AND task.deleted = 0
ORDER BY task.creation_date DESC ;
end$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure get_all_task_or_user_by_id
-- -----------------------------------------------------

DELIMITER $$
USE `projectdb`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_task_or_user_by_id`(IN p_user_id int)
BEGIN
SELECT task.id,
       task.category_id,
       task.name,
       task.description,
       task.creation_date,
       task.due_date,
       task.status,
       category.name  AS categoryName,
       category.color AS categoryColor,
       task.deleted
FROM task
         INNER JOIN category ON task.category_id = category.id
         INNER JOIN user ON category.user_id = user.id
WHERE user.id = p_user_id
  AND task.deleted = 0
ORDER BY task.creation_date DESC;
end$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure get_category_name
-- -----------------------------------------------------

DELIMITER $$
USE `projectdb`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_category_name`(IN p_user_id int)
BEGIN
SELECT category.name, category.id
FROM category
WHERE user_id = p_user_id AND category.deleted = 0;
end$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure insert_category
-- -----------------------------------------------------

DELIMITER $$
USE `projectdb`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_category`(
    IN p_categoryName VARCHAR(50),
    IN p_user_id int,
    IN p_color VARCHAR(20)
)
BEGIN
INSERT INTO category (name, user_id, color)
VALUES (p_categoryName, p_user_id, p_color);
end$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure insert_task_into_category
-- -----------------------------------------------------

DELIMITER $$
USE `projectdb`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_task_into_category`(
    IN p_user_id int,
    IN p_category_name int,
    IN p_task_name int,
    IN p_task_description text,
    IN p_task_due_date date,
    IN p_task_status enum ('pendiente', 'en progreso', 'completada')
)
BEGIN
    DECLARE category_id INT;

SELECT category.id INTO category_id
FROM category
WHERE user_id = p_user_id AND name = p_category_name;

INSERT INTO task (category_id, name, description, due_date, status)
VALUES (category_id, p_task_name, p_task_description, p_task_due_date, p_task_status);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure login_user
-- -----------------------------------------------------

DELIMITER $$
USE `projectdb`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `login_user`(
    IN p_username VARCHAR(50),
    IN p_password VARCHAR(100)
)
BEGIN
    DECLARE user_id INT;
    DECLARE user_password VARCHAR(100);
    DECLARE login_success BOOLEAN;
    DECLARE hashed_password VARCHAR(32);

SELECT MD5(p_password) INTO hashed_password;

SELECT id, password INTO user_id, user_password
FROM user
WHERE username = p_username
    LIMIT 1;

IF user_id IS NULL THEN
        SET login_success = FALSE;
ELSE
        IF user_password = hashed_password THEN
            SET login_success = TRUE;
ELSE
            SET login_success = FALSE;
END IF;
END IF;

SELECT login_success AS login_result, user_id AS user_id;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure register_user
-- -----------------------------------------------------

DELIMITER $$
USE `projectdb`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `register_user`(
    IN p_full_name VARCHAR(100),
    IN p_email VARCHAR(100),
    IN p_password VARCHAR(100),
    IN p_username VARCHAR(50)
)
BEGIN
INSERT INTO user (full_name, email, username, password)
VALUES (p_full_name, p_email, p_username, p_password);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure update_task_status
-- -----------------------------------------------------

DELIMITER $$
USE `projectdb`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_task_status`(
    IN p_task_id int,
    IN p_new_status enum  ('pendiente', 'en progreso', 'completada'))
BEGIN
UPDATE task
SET status = p_new_status
WHERE id = p_task_id;
END$$

DELIMITER ;
USE `projectdb`;

DELIMITER $$
USE `projectdb`$$
CREATE
DEFINER=`root`@`localhost`
    TRIGGER `projectdb`.`encrypt_password`
    BEFORE INSERT ON `projectdb`.`user`
    FOR EACH ROW
BEGIN
    SET NEW.password = MD5(NEW.password);
END$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;