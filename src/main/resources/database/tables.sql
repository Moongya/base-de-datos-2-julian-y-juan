-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema projectdb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema projectdb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `projectdb` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `projectdb` ;

-- -----------------------------------------------------
-- Table `projectdb`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectdb`.`user` (
                                                  `id` INT NOT NULL AUTO_INCREMENT,
                                                  `full_name` VARCHAR(50) NULL DEFAULT NULL,
                                                  `email` VARCHAR(100) NULL DEFAULT NULL,
                                                  `username` VARCHAR(45) NULL DEFAULT NULL,
                                                  `password` VARCHAR(150) NULL DEFAULT NULL,
                                                  `status` ENUM('block', 'allow') NULL DEFAULT 'allow',
                                                  PRIMARY KEY (`id`),
                                                  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
                                                  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE,
                                                  INDEX `idx_users_username` (`username` ASC) VISIBLE)
    ENGINE = InnoDB
    AUTO_INCREMENT = 57
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `projectdb`.`category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectdb`.`category` (
                                                      `id` INT NOT NULL AUTO_INCREMENT,
                                                      `name` VARCHAR(50) NULL DEFAULT NULL,
                                                      `color` VARCHAR(45) NULL DEFAULT NULL,
                                                      `user_id` INT NULL DEFAULT NULL,
                                                      `deleted` TINYINT(1) NULL DEFAULT '0',
                                                      PRIMARY KEY (`id`),
                                                      INDEX `fk_category_user_idx` (`user_id` ASC) VISIBLE,
                                                      CONSTRAINT `fk_category_user`
                                                          FOREIGN KEY (`user_id`)
                                                              REFERENCES `projectdb`.`user` (`id`))
    ENGINE = InnoDB
    AUTO_INCREMENT = 5
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `projectdb`.`task`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectdb`.`task` (
                                                  `id` INT NOT NULL AUTO_INCREMENT,
                                                  `category_id` INT NULL DEFAULT NULL,
                                                  `name` VARCHAR(50) NULL DEFAULT NULL,
                                                  `description` TEXT NULL DEFAULT NULL,
                                                  `creation_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
                                                  `due_date` DATE NULL DEFAULT NULL,
                                                  `status` ENUM('pending', 'in progress', 'completed') NULL DEFAULT 'pending',
                                                  `deleted` TINYINT(1) NULL DEFAULT '0',
                                                  PRIMARY KEY (`id`),
                                                  INDEX `fk_task_category1_idx` (`category_id` ASC) VISIBLE,
                                                  CONSTRAINT `fk_task_category1`
                                                      FOREIGN KEY (`category_id`)
                                                          REFERENCES `projectdb`.`category` (`id`))
    ENGINE = InnoDB
    AUTO_INCREMENT = 14
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_0900_ai_ci;


