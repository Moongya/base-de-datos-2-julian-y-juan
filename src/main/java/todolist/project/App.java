package todolist.project;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application {

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage stage) throws IOException {
    FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/fxml/Login-view.fxml"));
    Scene scene = new Scene(fxmlLoader.load(), 1200, 720);
    stage.setTitle("To-Do List - Login");
    stage.setScene(scene);
    stage.show();
  }
}
