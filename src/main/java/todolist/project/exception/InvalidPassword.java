package todolist.project.exception;

public class InvalidPassword extends InvalidInputForm {
  public InvalidPassword(String message) {
    super(message);
  }
}
