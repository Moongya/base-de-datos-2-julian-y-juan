package todolist.project.exception;

public class EmptyInputException extends TodoAppException {
  public EmptyInputException(String message) {
    super(message);
  }
}
