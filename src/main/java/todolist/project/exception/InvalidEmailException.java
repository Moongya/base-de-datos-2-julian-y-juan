package todolist.project.exception;

public class InvalidEmailException extends InvalidInputForm {
  public InvalidEmailException(String message) {
    super(message);
  }
}
