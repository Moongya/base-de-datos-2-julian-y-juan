package todolist.project.exception;

public class InvalidInputForm extends TodoAppException {
  public InvalidInputForm(String message) {
    super(message);
  }

  public InvalidInputForm() {
    super("the form field has an invalid value");
  }
}
