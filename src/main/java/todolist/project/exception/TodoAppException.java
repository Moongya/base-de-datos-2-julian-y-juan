package todolist.project.exception;

public class TodoAppException extends Exception {
  public TodoAppException(String message) {
    super(message);
  }

  public TodoAppException() {
    super("Application Error");
  }
}
