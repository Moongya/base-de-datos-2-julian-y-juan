package todolist.project.exception;

public class ConnectionDatabaseError extends TodoAppException{

  public ConnectionDatabaseError() {
    super("Error to connect to data base");
  }

  public ConnectionDatabaseError(String message) {
    super(message);
  }
}
