package todolist.project.exception;

public class RegisterDatabaseError extends TodoAppException {
  public RegisterDatabaseError() {
    super("Error while registered new user");
  }

  public RegisterDatabaseError(String message) {
    super(message);
  }
}
