package todolist.project.utilities;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/** This class provides methods for encrypting passwords using the MD5 hashing algorithm. */
public class EncryptedClass {

  private final String originPassword;
  private String encryptedPassword;

  /**
   * Constructs an instance of the EncryptedClass with the given original password.
   *
   * @param originPassword The original password to be encrypted.
   */
  public EncryptedClass(String originPassword) {
    this.originPassword = originPassword;
  }

  /**
   * Encrypts the original password using the MD5 hashing algorithm and returns the encrypted
   * password.
   *
   * @return The encrypted password as a string.
   */
  public String getEncryptedPassword() {
    try {
      MessageDigest messageDigest = MessageDigest.getInstance("MD5");
      messageDigest.update(originPassword.getBytes());
      byte[] bytes = messageDigest.digest();
      StringBuilder hexString = new StringBuilder();
      for (byte toByte : bytes) {
        hexString.append(Integer.toString((toByte & 0xff) + 0x100, 16).substring(1));
      }
      encryptedPassword = hexString.toString();
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return encryptedPassword;
  }
}
