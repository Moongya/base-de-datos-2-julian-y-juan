package todolist.project.utilities;

/** This class contains constant values used throughout the application. */
public class Constants {
  /** The URL of the database. */
  public static final String DB_URL = "jdbc:mysql://localhost:3306/projectdb";

  /** The username for the database. */
  public static final String DB_USER = "root";

  /** The password for the database. */
  public static final String DB_PASSWORD = "4769";

  /** The URL path for the Register view. */
  public static final String URL_REGISTER = "/fxml/Register-view.fxml";

  /** The URL path for the Login view. */
  public static final String URL_LOGIN = "/fxml/Login-view.fxml";

  /** The URL path for the Sidebar view. */
  public static final String URL_SIDE_BAR = "/fxml/SideBar-View.fxml";

  /** The URL path for the Task Info Cards view. */
  public static final String URL_INFO_CADS = "/fxml/ThumbnailTask-view.fxml";

  /** The URL path for the Create Task view. */
  public static final String URL_CREATE_TASK = "/fxml/CreateNewTaskAndCategory-view.fxml";

  /** The URL path for the Edit Task view. */
  public static final String URL_EDIT_TASK = "/fxml/EditTask-view.fxml";
}
