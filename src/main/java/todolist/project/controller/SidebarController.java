package todolist.project.controller;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import todolist.project.App;
import todolist.project.exception.TodoAppException;
import todolist.project.model.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static todolist.project.utilities.Constants.*;

/** Controller class for managing the sidebar of the todo list application. */
public class SidebarController extends TodoAppController {

  private static User user;

  @FXML private GridPane gridTask;
  @FXML private Text textTaskShow;
  @FXML private TextField txtSearchName;
  @FXML private ScrollPane scrollPaneTaskCategories;
  @FXML private AnchorPane anchorPane;
  @FXML private BorderPane borderPane;
  @FXML private VBox managementButtonsContainer;
  private Integer numElementRightMenu;
  private Button deleteCategoryButton;
  private double completedTasks;
  private double totalTasks;

  @FXML
  public void initialize() {
    user = this.getCurrentUser();
    numElementRightMenu = managementButtonsContainer.getChildren().size();
    txtSearchName.setOnKeyPressed(
        event -> {
          if (event.getCode() == KeyCode.ENTER) {
            search(txtSearchName.getText());
          }
        });
    try {
      TaskCategoryModel taskCategoryModel = new TaskCategoryModel();
      setTaskCategories(taskCategoryModel.getTaskCategoryName(user));
      TaskModel taskModel = new TaskModel();
      showInfoCards(getAllTaskModel(taskModel));
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
    deleteCategoryButton = new Button();
    deleteCategoryButton.setText("Delete this category");
    deleteCategoryButton.getStyleClass().add("Management-Buttons");
    deleteCategoryButton.setOnMouseClicked(
        event -> {
          TaskCategoryModel taskCategoryModel = null;
          try {
            taskCategoryModel = new TaskCategoryModel();
          } catch (TodoAppException e) {
            throw new RuntimeException(e);
          }
          try {
            taskCategoryModel.deleteCategory(this.getTaskCategory());
          } catch (SQLException e) {
            throw new RuntimeException(e);
          }
          List<TaskCategory> categories = this.getTaskCategories();
          categories.remove(this.getTaskCategory());
          this.setTaskCategories(categories);
          VBox content = (VBox) scrollPaneTaskCategories.getContent();
          for (Node node : content.getChildren()) {
            if (node instanceof Label) {
              Label label = (Label) node;
              if (label.getText().equals(this.getTaskCategory().getCategoryName())) {
                content.getChildren().remove(node);
                try {
                  goToMainPage(event);
                } catch (TodoAppException e) {
                  throw new RuntimeException(e);
                }
                break;
              }
            }
          }
        });
  }

  @FXML
  private void goToMainPage(MouseEvent event) throws TodoAppException {
    borderPane.setCenter(anchorPane);
    this.setTaskCategory(null);
    if (managementButtonsContainer.getChildren().contains(deleteCategoryButton)) {
      managementButtonsContainer.getChildren().remove(deleteCategoryButton);
    }
    setMainPage();
  }

  @FXML
  public void goToCompletedTasks(MouseEvent event) {
    if (this.getTaskCategory() == null) {
      getCompletedTasks(getAllTask());
    } else {
      getCompletedTasks(getTaskCurrentCategories());
    }
  }

  public void search(String taskName) {
    try {
      List<Task> result = new ArrayList<>();

      if (this.getTaskCategory() != null) {
        List<Task> tasks = this.getAllTaskByCategory(new TaskModel(), getTaskCategory());
        if (taskName.isEmpty()) {
          result = tasks;
        } else {
          for (Task task : tasks) {
            if (task.getTaskTittle().contains(taskName)) {
              result.add(task);
            }
          }
        }
      } else {
        if (taskName.isEmpty()) {
          result = new ArrayList<>(this.getAllTask());
        } else {
          for (Task task : this.getAllTask()) {
            if (task.getTaskTittle().contains(taskName)) {
              result.add(task);
            }
          }
        }
      }
      showInfoCards(result);
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  @FXML
  public void goToCreateNew(MouseEvent event) {
    loadPage(URL_CREATE_TASK);
  }

  @FXML
  public void showCompletedTask(MouseEvent event) {}

  @FXML
  public void goToLogOut(MouseEvent event) {
    try {
      FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(URL_LOGIN));
      Scene scene = new Scene(fxmlLoader.load(), 1200, 720);
      Stage stage = (Stage) borderPane.getScene().getWindow();
      stage.setTitle("To-Do List - Login");
      stage.setScene(scene);
      stage.show();
      user = null;
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
  }

  private void loadPage(String page) {
    Parent root = null;
    FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(page));
    try {
      root = fxmlLoader.load();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    borderPane.setCenter(root);
  }

  public void setMainPage() {
    ObservableList<Node> list = managementButtonsContainer.getChildren();
    if (list.size() == numElementRightMenu + 1) {}
    textTaskShow.setText("All task");
    try {
      TaskModel taskModel = new TaskModel();
      showInfoCards(getAllTaskModel(taskModel));
    } catch (TodoAppException | IOException e) {
      System.out.println(e.getMessage());
    }
  }

  private List<Task> getAllTaskModel(TaskModel taskModel) throws TodoAppException, IOException {
    List<Task> tasks = taskModel.getAllTaskOrUserById(user);
    this.setAllTask(new HashSet<>(tasks));
    completedTasks = showInfoCards(tasks);
    totalTasks = tasks.size();
    return tasks;
  }

  private List<Task> getAllTaskByCategory(TaskModel taskModel, TaskCategory taskCategory)
      throws TodoAppException, IOException {
    List<Task> tasks = taskModel.getAllTaskByCategoryId(taskCategory);
    Set<Task> allTasks = this.getAllTask();
    for (Task objTask : tasks) {
      if (!allTasks.contains(objTask)) {
        allTasks.add(objTask);
      }
    }
    this.setAllTask(allTasks);
    completedTasks = showInfoCards(tasks);
    totalTasks = tasks.size();
    return tasks;
  }

  private Set<Task> getTaskCurrentCategories() {
    Set<Task> tasksOfCategories = new HashSet<>();
    for (Task task : getAllTask()) {
      if (task.getCategoryId() == getTaskCategory().getId()) {
        tasksOfCategories.add(task);
      }
    }
    return tasksOfCategories;
  }

  private int showInfoCards(List<Task> tasks) throws IOException {
    gridTask.getChildren().clear();
    int completedTasks = 0;
    int row = 0;
    int col = 0;
    int maxCol = 3;

    for (Task taskItem : tasks) {
      FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(URL_INFO_CADS));
      Node node = fxmlLoader.load();
      ThumbnailTaskController controller = fxmlLoader.getController();
      controller.setTaskThumbnail(taskItem);

      gridTask.add(node, col, row);

      col++;
      if (col >= maxCol) {
        col = 0;
        row++;
      }

      if (taskItem.getStatus().equals("completed")) {
        completedTasks++;
      }
    }
    return completedTasks;
  }

  private void getCompletedTasks(Set<Task> taskCategories) {
    List<Task> tasks = new ArrayList<>();
    for (Task task : taskCategories) {
      if (task.getStatus().equals("completed")) {
        tasks.add(task);
      }
    }
    try {
      showInfoCards(tasks);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
