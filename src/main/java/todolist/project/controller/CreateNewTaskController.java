package todolist.project.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import todolist.project.exception.TodoAppException;
import todolist.project.model.TaskCategory;
import todolist.project.model.TaskCategoryModel;
import todolist.project.model.TaskModel;
import todolist.project.model.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/** Controller class for creating a new task. */
public class CreateNewTaskController extends TodoAppController {

  private final User currentUser = this.getCurrentUser();
  TaskModel taskModel = new TaskModel();
  TaskCategoryModel taskCategoryModel = new TaskCategoryModel();
  private Date taskDueDate;
  private Integer taskCategoryId;
  private String taskName;
  private String taskDescription;

  @FXML private VBox boxCategory;

  @FXML private ColorPicker colorPicker;

  @FXML private Text txtErrorMessage;

  @FXML private Text txtErrorMessageCategory;

  @FXML private TextField txtCategoryName;

  @FXML private TextField txtTaskDateDay;

  @FXML private TextField txtTaskDateMonth;

  @FXML private TextField txtTaskDateYear;

  @FXML private TextField txtTaskDescription;

  @FXML private TextField txtTaskName;

  private String taskCategoryName;
  private String color;

  public CreateNewTaskController() throws TodoAppException {}

  public void initialize() throws TodoAppException {
    clearSpaces();
    clearCategory();
    TaskCategoryModel taskCategoryModel = new TaskCategoryModel();
    printList(taskCategoryModel.getTaskCategoryName(currentUser));
  }

  @FXML
  public void createTask(MouseEvent event) throws ParseException, TodoAppException {
    if (verifyTask()
        && verifyDate(
            txtTaskDateDay.getText(), txtTaskDateMonth.getText(), txtTaskDateYear.getText())
        && taskCategoryId != null) {
      clearSpaces();
      setErrorMessage("Successful");
      taskModel.insertTask(taskCategoryId, taskName, taskDescription, taskDueDate);
    } else if (taskCategoryId == null) {
      setErrorMessage("You have not selected a category");
    }
  }

  private boolean verifyTask() {
    if (txtTaskName.getText().length() > 30) {
      setErrorMessage("The name is very long");
      return false;
    } else if (txtTaskName.getText().isEmpty()) {
      setErrorMessage("Insert a tittle for a task");
      return false;
    } else if (txtTaskDescription.getText().length() > 50) {
      setErrorMessage("The description is very long");
    }
    taskDescription = txtTaskDescription.getText();
    taskName = txtTaskName.getText();
    return true;
  }

  public boolean verifyDate(String dayStr, String monthStr, String yearStr) throws ParseException {
    Calendar calendar = Calendar.getInstance();
    int year = calendar.get(Calendar.YEAR);
    String dateFormat = "dd-MM-yyyy";

    if (dayStr.isEmpty() || monthStr.isEmpty() || yearStr.isEmpty()) {
      setErrorMessage("Enter a complete date");
      return false;
    }

    if (Integer.parseInt(yearStr) < year) {
      setErrorMessage("Wrong year");
      return false;
    }

    String dateString = dayStr + "-" + monthStr + "-" + yearStr;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
    simpleDateFormat.setLenient(false);

    try {
      taskDueDate = simpleDateFormat.parse(dateString);
      return true;
    } catch (ParseException e) {
      setErrorMessage("Invalid date");
      throw e;
    }
  }

  private void printList(List<TaskCategory> taskCategories) {
    boxCategory.getChildren().clear();
    for (TaskCategory taskCategory : taskCategories) {
      Button button = new Button(taskCategory.getCategoryName());
      button.setOnAction(
          event -> {
            taskCategoryId = taskCategory.getId();
            setErrorMessage(
                "The "
                    + taskCategory.getCategoryName()
                    + " category has been selected successfully");
          });

      button.getStyleClass().add("button-category-name");
      boxCategory.getChildren().add(button);
    }
  }

  @FXML
  private void createCategory(MouseEvent event) throws TodoAppException {
    color = String.valueOf(colorPicker.getValue());
    if (verifyTaskCategoryName()) {
      taskCategoryModel.createCategory(taskCategoryName, currentUser, color);
      this.setTaskCategories(taskCategoryModel.getTaskCategoryName(this.getCurrentUser()));
      initialize();
    }
  }

  private boolean verifyTaskCategoryName() {
    if (txtTaskName.getText().equals(null)) {
      setErrorMessageCategory("Insert a name for the category");
      return false;
    } else if (txtTaskName.getText().length() > 50) {
      setErrorMessageCategory("The tittle is very long");
      return false;
    }
    taskCategoryName = txtCategoryName.getText();
    return true;
  }

  private void setErrorMessage(String text) {
    txtErrorMessage.setText(text);
  }

  private void setErrorMessageCategory(String text) {
    txtErrorMessageCategory.setText(text);
  }

  private void clearCategory() {
    txtCategoryName.setText("");
    txtErrorMessageCategory.setText("");
  }

  private void clearSpaces() {
    txtTaskName.setText("");
    txtTaskDescription.setText("");
    txtErrorMessage.setText("");
    txtTaskDateDay.setText("");
    txtTaskDateMonth.setText("");
    txtTaskDateYear.setText("");
  }
}
