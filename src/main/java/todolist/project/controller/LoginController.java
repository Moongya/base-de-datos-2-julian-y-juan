package todolist.project.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import todolist.project.App;
import todolist.project.exception.TodoAppException;
import todolist.project.model.LoginModel;
import todolist.project.model.User;

import java.io.IOException;

import static todolist.project.utilities.Constants.URL_REGISTER;
import static todolist.project.utilities.Constants.URL_SIDE_BAR;

/** Controller class for the login form. */
public class LoginController extends TodoAppController {

  @FXML private TextField username;

  @FXML private PasswordField password;

  @FXML private Button login;

  @FXML private Hyperlink register;

  @FXML
  protected void registerRedirect() {
    try {
      FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(URL_REGISTER));
      Scene scene = new Scene(fxmlLoader.load(), 1200, 720);
      Stage stage = (Stage) register.getScene().getWindow();
      stage.setTitle("Register");
      stage.setScene(scene);
      stage.show();
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
  }

  @FXML
  protected void handleLogin() {
    try {
      String loginUsername = username.getText();
      String loginPassword = password.getText();

      LoginModel loginModel = new LoginModel();

      if (loginModel.checkInformationToLogin(loginUsername, loginPassword)) {
        clearFields();
        loadMainView(loginModel.getUser());
      }

    } catch (TodoAppException e) {
      showError("error", e.getMessage());
    }
  }

  private void loadMainView(User user) {
    try {
      this.setCurrentUser(user);
      FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(URL_SIDE_BAR));
      Scene scene = new Scene(fxmlLoader.load(), 1200, 720);
      Stage stage = (Stage) username.getScene().getWindow();
      stage.setTitle("To-Do List - Main");
      stage.setScene(scene);
      stage.show();
    } catch (IOException e) {
      showError("Error loading main view", e.getMessage());
    }
  }

  private void showError(String tittle, String message) {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle(tittle);
    alert.setHeaderText(null);
    alert.setContentText(message);
    alert.showAndWait();
  }

  private void clearFields() {
    username.clear();
    password.clear();
  }
}
