package todolist.project.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import todolist.project.App;
import todolist.project.exception.TodoAppException;
import todolist.project.model.Task;
import todolist.project.model.TaskModel;

import java.io.IOException;
import java.util.Set;

import static todolist.project.utilities.Constants.URL_EDIT_TASK;

/**
 * Controller class for managing task thumbnails in the todo list application.
 */
public class ThumbnailTaskController extends TodoAppController {
  private Task task;
  private SidebarController sidebarController;

  @FXML private Button btnDelete;

  @FXML private Label labelTag;

  @FXML private Button btnStatus;

  @FXML private Text txtTaskName;

  @FXML private Text txtTaskDescription;

  @FXML private AnchorPane taskContainer;

  @FXML private Text txtDateField;

  protected void setTxtTaskName(String text) {
    txtTaskName.setText(text);
  }

  protected void setTxtTaskDescription(String text) {
    txtTaskDescription.setText(text);
  }

  public void setTaskThumbnail(Task task) {
    this.task = task;
    txtTaskName.setText(task.getTaskTittle());
    this.labelTag.setText(task.getCategoryName());
    if (task.getCategoryColor() != null) {
      this.labelTag.setStyle(
          "-fx-background-radius: 50; -fx-padding: 5 10 5 10;-fx-background-color: "
              + setTagColor(task.getCategoryColor())
              + ";");
    }
    if (!(task.getTaskDescription() == null)) {
      txtTaskDescription.setText(task.getTaskDescription());
    } else {
      txtTaskDescription.setText("");
    }
    setBtnStatus();

    txtDateField.setText(task.getTaskDueDate() != null ? task.getTaskDueDate().toString() : "");
  }

  public String setTagColor(String inputColor) {
    if (inputColor == null || inputColor.isEmpty()) {
      return null;
    }

    if (inputColor.startsWith("0x") && inputColor.length() == 10) {
      String red = inputColor.substring(2, 4);
      String green = inputColor.substring(4, 6);
      String blue = inputColor.substring(6, 8);
      return "#" + red + green + blue;
    }

    return "#" + inputColor;
  }

  private void setBtnStatus() {
    btnStatus.getStyleClass().removeAll("color_red", "color_yellow", "color_green");
    btnStatus.setText(task.getStatus());
    switch (task.getStatus()) {
      case "pending":
        btnStatus.getStyleClass().add("color_red");
        break;
      case "in progress":
        btnStatus.getStyleClass().add("color_yellow");
        break;
      case "completed":
        btnStatus.getStyleClass().add("color_green");
        break;
    }
  }

  @FXML
  public void changeTaskStatus(MouseEvent event) throws TodoAppException {
    switch (task.getStatus()) {
      case "pending":
        task.changeTask("status", "in progress", task.getId());
        break;
      case "in progress":
        task.changeTask("status", "completed", task.getId());
        break;
      case "completed":
        task.changeTask("status", "pending", task.getId());
    }
    Set<Task> tasks = getAllTask();
    for (Task taskItem : tasks) {
      if (this.task.getId() == taskItem.getId()) {
        tasks.remove(taskItem);
        tasks.add(this.task);
        setAllTask(tasks);
        break;
      }
    }
    setBtnStatus();
  }

  @FXML
  public void editTask(MouseEvent event) {
    Scene scene = taskContainer.getScene();
    Stage stage = (Stage) scene.getWindow();
    showPopUpWindow(stage);
  }

  public void deletedTask(ActionEvent event) {
    try {
      TaskModel taskModel = new TaskModel();
      taskModel.deletedTask(this.task);
      Set<Task> userTask = this.getAllTask();
      for (Task objTask : userTask) {
        if (objTask.equals(this.task)) {
          userTask.remove(objTask);
          this.setAllTask(userTask);
          break;
        }
      }
      GridPane parent = (GridPane) taskContainer.getParent();
      parent.getChildren().remove(taskContainer);
    } catch (TodoAppException e) {
      System.out.println(e.getMessage());
    }
  }

  private void showPopUpWindow(Stage primaryStage) {
    try {
      FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(URL_EDIT_TASK));
      Parent root = fxmlLoader.load();
      EditTaskViewController editTaskViewController = fxmlLoader.getController();
      editTaskViewController.setTaskId(task.getId());
      editTaskViewController.setTaskController(this);
      Scene scene = new Scene(root);
      Stage popUpWindow = new Stage();
      popUpWindow.initModality(Modality.WINDOW_MODAL);
      popUpWindow.initOwner(primaryStage);
      popUpWindow.setTitle("Edit Task");
      popUpWindow.setScene(scene);
      popUpWindow.show();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
