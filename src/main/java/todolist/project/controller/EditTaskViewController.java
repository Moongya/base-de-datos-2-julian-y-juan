package todolist.project.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import todolist.project.exception.TodoAppException;
import todolist.project.model.Task;
import todolist.project.model.TaskModel;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/** Controller class for editing a task. */
public class EditTaskViewController extends TodoAppController {
  private Integer taskId;

  private ThumbnailTaskController taskController;

  @FXML private DatePicker dateInput;

  @FXML private TextField txtNameInput;

  @FXML private TextArea txtDescriptionInput;

  @FXML private Button editButton;

  @FXML private Label errorLabel;

  public void setTaskId(Integer taskId) {
    this.taskId = taskId;
  }

  public void setTaskController(ThumbnailTaskController taskController) {
    this.taskController = taskController;
  }

  @FXML
  public void editTask(ActionEvent event) {
    try {
      TaskModel taskModel = new TaskModel();
      if (!txtNameInput.getText().isEmpty()) {
        taskModel.updateTaskName(txtNameInput.getText(), taskId);
        taskController.setTxtTaskName(txtNameInput.getText());
        for (Task task : getAllTask()) {
          if (task.getId() == taskId) {
            task.setTaskTittle(txtNameInput.getText());
          }
        }
      }
      if (!txtDescriptionInput.getText().isEmpty()) {
        taskModel.updateTaskDescription(txtDescriptionInput.getText(), taskId);
        taskController.setTxtTaskDescription(txtDescriptionInput.getText());
        for (Task task : getAllTask()) {
          if (task.getId() == taskId) {
            task.setTaskDescription(txtDescriptionInput.getText());
          }
        }
      }
      if (dateInput.getValue() != null) {
        if (dateInput.getValue().isBefore(LocalDate.now())) {
          throw new TodoAppException("The date is before de current date");
        }
        LocalDate date = dateInput.getValue();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String formattedDate = date.format(dateTimeFormatter);
        LocalDate localDate = LocalDate.parse(formattedDate, dateTimeFormatter);
        java.util.Date dueDate = java.sql.Date.valueOf(localDate);
        taskModel.updateTaskDueDate(formattedDate, taskId);
        for (Task task : getAllTask()) {
          if (task.getId() == taskId) {
            task.setTaskDueDate(dueDate);
          }
        }
      }
      Stage stage = (Stage) editButton.getScene().getWindow();
      stage.close();
    } catch (TodoAppException e) {
      errorLabel.setTextFill(Color.RED);
      errorLabel.setText(e.getMessage());
    }
  }
}
