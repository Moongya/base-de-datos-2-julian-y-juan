package todolist.project.controller;

import todolist.project.model.Task;
import todolist.project.model.TaskCategory;
import todolist.project.model.User;

import java.util.List;
import java.util.Set;

/** Abstract class representing the controller for the todo list application. */
public abstract class TodoAppController {
  private static User currentUser;
  private static TaskCategory taskCategory;
  private static List<TaskCategory> taskCategories;
  private static Set<Task> allTask;

  protected void setCurrentUser(User user) {
    currentUser = user;
  }

  protected User getCurrentUser() {
    return currentUser;
  }

  protected List<TaskCategory> getTaskCategories() {
    return taskCategories;
  }

  protected void setTaskCategories(List<TaskCategory> taskCategories) {
    TodoAppController.taskCategories = taskCategories;
  }

  protected TaskCategory getTaskCategory() {
    return taskCategory;
  }

  protected void setTaskCategory(TaskCategory taskCategory) {
    TodoAppController.taskCategory = taskCategory;
  }

  public Set<Task> getAllTask() {
    return allTask;
  }

  public void setAllTask(Set<Task> allTask) {
    TodoAppController.allTask = allTask;
  }
}
