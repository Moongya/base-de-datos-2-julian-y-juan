package todolist.project.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import todolist.project.App;
import todolist.project.exception.TodoAppException;
import todolist.project.model.RegisterModel;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import static todolist.project.utilities.Constants.URL_LOGIN;

/** Controller class for the registration form. */
public class RegisterController implements Initializable {

  @FXML private TextField fullName;
  @FXML private TextField email;
  @FXML private TextField username;
  @FXML private PasswordField password;
  @FXML private PasswordField confirmPassword;
  @FXML private Button registeredBtn;
  @FXML private Label alertRegistered;

  @FXML
  protected void registerUser(ActionEvent event) {
    RegisterModel registerModel =
        new RegisterModel(
            username.getText(),
            fullName.getText(),
            email.getText(),
            password.getText(),
            confirmPassword.getText());
    try {
      registerModel.validateEmpty();
      registerModel.isValidUsername();
      registerModel.isValidEmail();
      registerModel.isValidPassword();
      registerModel.checkPasswordConfirm();
      registerModel.registerUser();
      alertRegistered.setTextFill(Color.color(0, 0, 1));
      alertRegistered.setText("Successful registration");
      resetTextInputs();
    } catch (TodoAppException e) {
      alertRegistered.setText(e.getMessage());
    }
  }

  @FXML
  protected void loginRedirect() {
    try {
      FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(URL_LOGIN));
      Scene scene = new Scene(fxmlLoader.load(), 1200, 720);
      Stage stage = (Stage) registeredBtn.getScene().getWindow();
      stage.setTitle("Login");
      stage.setScene(scene);
      stage.show();
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
  }

  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {
    alertRegistered.setText("");
  }

  private void resetTextInputs() {
    fullName.setText("");
    email.setText("");
    username.setText("");
    password.setText("");
    confirmPassword.setText("");
  }
}
