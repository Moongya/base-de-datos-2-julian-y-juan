package todolist.project.model;

import todolist.project.exception.TodoAppException;
import todolist.project.utilities.EncryptedClass;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/** Model class for user login in the todo list application. */
public class LoginModel {
  private final Connection connection;
  private User user;

  public LoginModel() throws TodoAppException {
    this.connection = DataBaseManager.getConnection();
  }

  public User getUser() {
    return this.user;
  }

  /**
   * Checks if the provided username and password match a user in the database.
   *
   * @param username The username provided during login.
   * @param password The password provided during login.
   * @return True if the login information is correct, false otherwise.
   * @throws TodoAppException If an error occurs during the login process.
   */
  public boolean checkInformationToLogin(String username, String password) throws TodoAppException {
    try {
      ResultSet resultSet = loginUser(username, password).executeQuery();

      if (resultSet.next()) {
        if (resultSet.getBoolean("login_result")) {
          this.user = new User(resultSet.getInt("user_id"), username);
          return true;
        } else {
          throw new TodoAppException("Incorrect username or password");
        }
      }

    } catch (SQLException e) {
      throw new TodoAppException("Login database error");
    }
    return false;
  }

  private CallableStatement loginUser(String username, String password) throws SQLException {
    CallableStatement statement = connection.prepareCall("{CALL login_user(?, ?)}");

    statement.setString(1, username);
    EncryptedClass encrypted = new EncryptedClass(password);
    statement.setString(2, encrypted.getEncryptedPassword());
    return statement;
  }
}
