package todolist.project.model;

/**
 * Represents a category of tasks in the todo list application.
 */
public class TaskCategory {

  private final String categoryName;
  private final Integer id;

  public TaskCategory(String categoryName, Integer id) {
    this.categoryName = categoryName;
    this.id = id;
  }

  public String getCategoryName() {
    return categoryName;
  }

  public Integer getId() {
    return id;
  }

  @Override
  public String toString() {
    return this.categoryName;
  }
}
