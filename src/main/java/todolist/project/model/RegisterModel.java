package todolist.project.model;

import todolist.project.exception.*;
import todolist.project.utilities.EncryptedClass;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.Normalizer;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Model class for user registration in the todo list application. */
public class RegisterModel {
  private final String username;
  private final String fullName;
  private final String email;
  private final String password;
  private final String confirmPassword;

  public RegisterModel(
      String username, String fullName, String email, String password, String confirmPassword) {
    this.username = username;
    this.fullName = fullName;
    this.email = email;
    this.password = password;
    this.confirmPassword = confirmPassword;
  }

  /**
   * Validates if any required field is empty during registration.
   *
   * @throws TodoAppException If any required field is empty.
   */
  public void validateEmpty() throws TodoAppException {
    if (email.isEmpty()
        || fullName.isEmpty()
        || password.isEmpty()
        || username.isEmpty()
        || confirmPassword.isEmpty()) {
      StringBuilder errorMsg = new StringBuilder("The following fields are empty: ");

      if (email.isEmpty()) {
        errorMsg.append("Email, ");
      }
      if (fullName.isEmpty()) {
        errorMsg.append("Full name, ");
      }
      if (password.isEmpty()) {
        errorMsg.append("Password, ");
      }
      if (username.isEmpty()) {
        errorMsg.append("Username, ");
      }
      if (confirmPassword.isEmpty()) {
        errorMsg.append("Password confirmation, ");
      }

      errorMsg.deleteCharAt(errorMsg.length() - 2);
      throw new EmptyInputException(errorMsg.toString());
    }
  }

  /**
   * Validates the username provided during registration.
   *
   * @throws TodoAppException If the username is invalid.
   */
  public void isValidUsername() throws TodoAppException {
    if (username.contains(" ")) {
      throw new InvalidInputForm("The username cannot contain whitespace.");
    }

    String specialCharacters = "!@#$%^&*()-_+=<>?/|";
    for (char character : specialCharacters.toCharArray()) {
      if (username.contains(Character.toString(character))) {
        throw new InvalidInputForm("The username cannot contain special characters or symbols.");
      }
    }

    if (username.startsWith(".") || username.endsWith(".")) {
      throw new InvalidInputForm("The username cannot begin or end with a period.");
    }

    String normalized = Normalizer.normalize(username, Normalizer.Form.NFD);
    if (!normalized.matches("\\p{ASCII}+")) {
      throw new InvalidInputForm("The username cannot contain accents or diacritics.");
    }

    String punctuation = ",;:'\"[]{}()<>?!";
    for (char character : punctuation.toCharArray()) {
      if (username.contains(Character.toString(character))) {
        throw new InvalidInputForm(
            "The username cannot contain punctuation characters or miscellaneous symbols.");
      }
    }
  }

  /**
   * Validates the email address provided during registration.
   *
   * @throws TodoAppException If the email address is invalid.
   */
  public void isValidEmail() throws TodoAppException {
    String regex = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(email);
    boolean isValid = matcher.matches();
    if (!isValid && email.isEmpty()) {
      throw new EmptyInputException("The email cannot be empty");
    } else if (!isValid) {
      throw new InvalidEmailException(this.email + " It's not valid");
    }
  }

  /**
   * Checks if the password and confirmPassword match during registration.
   *
   * @throws TodoAppException If the passwords do not match.
   */
  public void checkPasswordConfirm() throws TodoAppException {
    if (!Objects.equals(confirmPassword, password)) {
      throw new InvalidPassword("The password and confirmPassword are not the same");
    }
  }

  /**
   * Validates the password provided during registration.
   *
   * @throws TodoAppException If the password is invalid.
   */
  public void isValidPassword() throws TodoAppException {
    if (password.length() < 8) {
      throw new InvalidPassword("The password must be at least 8 characters");
    }
  }

  /**
   * Registers the user by storing their information in the database.
   *
   * @throws TodoAppException If an error occurs during registration.
   */
  public void registerUser() throws TodoAppException {
    try {
      Connection connection = DataBaseManager.getConnection();
      EncryptedClass encrypted = new EncryptedClass(password);
      String encryptedPassword = encrypted.getEncryptedPassword();
      PreparedStatement registerStatement =
          connection.prepareStatement("CALL register_user(?,?,?,?)");
      registerStatement.setString(1, fullName);
      registerStatement.setString(2, email);
      registerStatement.setString(3, encryptedPassword);
      registerStatement.setString(4, username);
      registerStatement.executeUpdate();
    } catch (SQLException e) {
      if (e.getMessage().contains("Data truncated " + "for column 'email'")
          || e.getMessage().contains("Duplicate entry '" + username + "' for key " + "'username'")
          || e.getMessage().contains("Duplicate entry '" + email + "' for key " + "'email'")) {
        System.out.println(e.getMessage());
        throw new RegisterDatabaseError("The email or User is already registered.");
      } else {
        throw new RegisterDatabaseError();
      }
    }
  }
}
