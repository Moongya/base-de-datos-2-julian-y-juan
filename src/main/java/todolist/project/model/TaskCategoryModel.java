package todolist.project.model;

import todolist.project.exception.RegisterDatabaseError;
import todolist.project.exception.TodoAppException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/** Model class for managing task categories in the todo list application. */
public class TaskCategoryModel {

  private final Connection connection;

  public TaskCategoryModel() throws TodoAppException {
    connection = DataBaseManager.getConnection();
  }

  /**
   * Retrieves task categories associated with a user.
   *
   * @param user The user for whom to retrieve task categories.
   * @return A list of task categories associated with the user.
   * @throws TodoAppException If an error occurs during database operation.
   */
  public List<TaskCategory> getTaskCategoryName(User user) throws TodoAppException {
    try {
      List<TaskCategory> taskCategories = new ArrayList<>();
      PreparedStatement preparedStatement =
          connection.prepareStatement("CALL get_category_name(?)");
      preparedStatement.setInt(1, user.getId());
      ResultSet resultSet = preparedStatement.executeQuery();

      while (resultSet.next()) {
        resultSet.getString("name").split(" ");
        taskCategories.add(new TaskCategory(resultSet.getString("name"), resultSet.getInt("id")));
      }
      preparedStatement.close();
      return taskCategories;
    } catch (SQLException e) {
      throw new RegisterDatabaseError(e.getMessage());
    }
  }

  /**
   * Deletes a task category from the database.
   *
   * @param taskCategory The task category to delete.
   * @throws SQLException If an SQL error occurs during the deletion operation.
   */
  public void deleteCategory(TaskCategory taskCategory) throws SQLException {
    PreparedStatement preparedStatement = connection.prepareStatement("CALL delete_category(?)");
    preparedStatement.setInt(1, taskCategory.getId());
    preparedStatement.executeUpdate();
    preparedStatement.close();
  }

  /**
   * Creates a new task category in the database.
   *
   * @param categoryName The name of the new task category.
   * @param user The user associated with the new task category.
   * @param color The color associated with the new task category.
   * @throws TodoAppException If an error occurs during database operation.
   */
  public void createCategory(String categoryName, User user, String color) throws TodoAppException {
    try {
      PreparedStatement preparedStatement =
          connection.prepareStatement("CALL insert_category(?,?,?)");
      preparedStatement.setString(1, categoryName);
      preparedStatement.setInt(2, user.getId());
      preparedStatement.setString(3, color);
      preparedStatement.executeUpdate();
      preparedStatement.close();

    } catch (SQLException e) {
      throw new RegisterDatabaseError(e.getMessage());
    }
  }
}
