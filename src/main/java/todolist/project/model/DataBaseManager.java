package todolist.project.model;

import todolist.project.exception.ConnectionDatabaseError;
import todolist.project.exception.TodoAppException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static todolist.project.utilities.Constants.*;

/** Class responsible for managing the database connection in the todo list application. */
public class DataBaseManager {
  private static Connection connection;

  private DataBaseManager() throws SQLException {
    connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
  }

  public static Connection getConnection() throws TodoAppException {
    if (connection == null) {
      try {
        new DataBaseManager();
      } catch (SQLException e) {
        throw new ConnectionDatabaseError();
      }
    }
    return connection;
  }
}
