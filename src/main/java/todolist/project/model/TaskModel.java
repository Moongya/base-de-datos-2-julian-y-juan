package todolist.project.model;

import todolist.project.exception.ConnectionDatabaseError;
import todolist.project.exception.RegisterDatabaseError;
import todolist.project.exception.TodoAppException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/** Model class for managing tasks in the todo list application. */
public class TaskModel {

  private final Connection connection;

  public TaskModel() throws TodoAppException {
    try {
      connection = DataBaseManager.getConnection();
    } catch (Exception e) {
      throw new ConnectionDatabaseError();
    }
  }

  /**
   * Executes a SQL query and retrieves tasks from the database.
   *
   * @param query The SQL query to execute.
   * @return A list of tasks retrieved from the database.
   * @throws TodoAppException If an error occurs during database operation.
   */
  public List<Task> executedQueryAndGetTasks(String query) throws TodoAppException {
    try {
      List<Task> tasks = new ArrayList<>();
      PreparedStatement statement = connection.prepareStatement(query);
      ResultSet result = statement.executeQuery();

      while (result.next()) {
        int categoryId = result.getInt("category_id");
        Date taskDueDate = result.getDate("due_date");
        int idTask = result.getInt("id");
        String taskName = result.getString("name");
        String taskDescription = result.getString("description");
        Date taskCreationDate = result.getDate("creation_date");
        String status = result.getString("status");
        String color = result.getString("categoryColor");
        String name = result.getString("categoryName");
        if (!(taskName.isEmpty() || status.equals(null))) {
          Task task =
              new Task(
                  idTask,
                  categoryId,
                  taskName,
                  taskDescription,
                  taskCreationDate,
                  taskDueDate,
                  status,
                  color,
                  name);
          tasks.add(task);
        }
      }
      return tasks;

    } catch (Exception e) {
      throw new RegisterDatabaseError(e.getMessage());
    }
  }

  public List<Task> getAllTaskOrUserById(User user) throws TodoAppException {
    String query = "CALL get_all_task_or_user_by_id(" + user.getId() + ")";
    return executedQueryAndGetTasks(query);
  }

  public List<Task> getAllTaskByCategoryId(TaskCategory taskCategory) throws TodoAppException {
    String query = "CALL get_all_task_by_category_id(" + taskCategory.getId() + ")";
    return executedQueryAndGetTasks(query);
  }

  public void insertTask(
      int taskCategoryId, String taskName, String taskDescription, Date taskDueDate)
      throws TodoAppException {
    try {
      String query =
          "INSERT INTO task (category_id, name, description, due_date, status) VALUES (?,?,?,?,'pending')";

      PreparedStatement preparedStatement = connection.prepareStatement(query);
      preparedStatement.setInt(1, taskCategoryId);
      preparedStatement.setString(2, taskName);
      preparedStatement.setString(3, taskDescription);
      java.sql.Date sqlDueDate = new java.sql.Date(taskDueDate.getTime());
      preparedStatement.setDate(4, sqlDueDate);
      preparedStatement.executeUpdate();
      preparedStatement.close();
    } catch (SQLException e) {
      throw new TodoAppException("Error inserting task: " + e.getMessage());
    }
  }

  public void deletedTask(Task task) {
    try {
      PreparedStatement preparedStatement = connection.prepareStatement("CALL delete_task(?)");
      preparedStatement.setInt(1, task.getId());
      preparedStatement.executeUpdate();
    } catch (SQLException e) {
      throw new RuntimeException(e.getMessage());
    }
  }

  public void updateTaskName(String newName, Integer id) throws TodoAppException {
    try {
      PreparedStatement preparedStatement =
          connection.prepareStatement("UPDATE task SET name = ? WHERE" + " id = ?");
      preparedStatement.setString(1, newName);
      preparedStatement.setInt(2, id);
      preparedStatement.executeUpdate();

    } catch (SQLException e) {
      throw new TodoAppException("Error when updating task");
    }
  }

  public void updateTaskDescription(String newDescription, Integer id) throws TodoAppException {
    try {
      PreparedStatement preparedStatement =
          connection.prepareStatement("UPDATE task SET description = ? WHERE" + " id = ?");
      preparedStatement.setString(1, newDescription);
      preparedStatement.setInt(2, id);
      preparedStatement.executeUpdate();
    } catch (SQLException e) {
      throw new TodoAppException("Error when updating task");
    }
  }

  public void updateTaskDueDate(String newDueDate, Integer id) throws TodoAppException {
    try {
      PreparedStatement preparedStatement =
          connection.prepareStatement("UPDATE task SET due_date = ? WHERE" + " id = ?");
      preparedStatement.setString(1, newDueDate);
      preparedStatement.setInt(2, id);
      preparedStatement.executeUpdate();
    } catch (SQLException e) {
      throw new TodoAppException("Error when updating task");
    }
  }
}
