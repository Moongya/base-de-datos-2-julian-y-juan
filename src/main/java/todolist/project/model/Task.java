package todolist.project.model;

import todolist.project.exception.RegisterDatabaseError;
import todolist.project.exception.TodoAppException;

import java.sql.PreparedStatement;
import java.util.Date;
import java.util.Objects;

/** Represents a task in the todo list application. */
public class Task {

  private int id;
  private int categoryId;
  private String taskTittle;
  private String taskDescription;
  private Date taskCreationDate;
  private Date taskDueDate;
  private String status;
  private String categoryName;
  private String categoryColor;

  public Task(
      int id,
      int categoryId,
      String taskTittle,
      String taskDescription,
      Date taskCreationDate,
      Date taskDueDate,
      String status,
      String categoryName,
      String categoryColor) {
    this.id = id;
    this.categoryId = categoryId;
    this.taskTittle = taskTittle;
    this.taskDescription = taskDescription;
    this.taskCreationDate = taskCreationDate;
    this.taskDueDate = taskDueDate;
    this.status = status;
    this.categoryName = categoryName;
    this.categoryColor = categoryColor;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(int categoryId) {
    this.categoryId = categoryId;
  }

  public String getTaskTittle() {
    return taskTittle;
  }

  public void setTaskTittle(String taskTittle) {
    this.taskTittle = taskTittle;
  }

  public String getTaskDescription() {
    return taskDescription;
  }

  public void setTaskDescription(String taskDescription) {
    this.taskDescription = taskDescription;
  }

  public Date getTaskCreationDate() {
    return taskCreationDate;
  }

  public void setTaskCreationDate(Date taskCreationDate) {
    this.taskCreationDate = taskCreationDate;
  }

  public Date getTaskDueDate() {
    return taskDueDate;
  }

  public void setTaskDueDate(Date taskDueDate) {
    this.taskDueDate = taskDueDate;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getCategoryName() {
    return categoryName;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }

  public String getCategoryColor() {
    return categoryColor;
  }

  public void setCategoryColor(String categoryColor) {
    this.categoryColor = categoryColor;
  }

  private void changeTask(String query) throws TodoAppException {
    try {
      PreparedStatement statement = DataBaseManager.getConnection().prepareStatement(query);
      statement.executeUpdate();
    } catch (Exception e) {
      throw new RegisterDatabaseError(e.getMessage());
    }
  }

  /**
   * Changes a specific field of the task in the database.
   *
   * @param field The field of the task to change.
   * @param value The new value for the field.
   * @param taskId The ID of the task to update.
   * @throws TodoAppException If an error occurs during database operation.
   */
  public void changeTask(String field, String value, int taskId) throws TodoAppException {
    String query = "UPDATE task SET " + field + " = '" + value + "' WHERE id = " + taskId;
    changeTask(query);
    switch (field) {
      case "status":
        setStatus(value);
    }
  }

  /**
   * Overrides the equals method to compare tasks based on their IDs.
   *
   * @param obj The object to compare with this task.
   * @return True if the tasks have the same ID, false otherwise.
   */
  @Override
  public boolean equals(Object obj) {
    if (obj == null | this.getClass() != obj.getClass()) {
      return false;
    }
    ;
    Task task = (Task) obj;
    return this.getId() == task.getId();
  }

  /**
   * Overrides the hashCode method to generate a hash code based on the task ID.
   *
   * @return The hash code value for this task.
   */
  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
