module todolist.project.todoapp {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires net.synedra.validatorfx;
    requires org.kordamp.bootstrapfx.core;
    requires java.sql;

    opens todolist.project to javafx.fxml;
    exports todolist.project;
    exports todolist.project.controller;
    opens todolist.project.controller to javafx.fxml;
}