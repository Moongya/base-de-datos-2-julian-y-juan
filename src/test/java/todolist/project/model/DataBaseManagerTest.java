package todolist.project.model;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import todolist.project.exception.TodoAppException;
import todolist.project.model.DataBaseManager;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

public class DataBaseManagerTest {
    @Test
    public void testGetConnection() {
        try {
            Connection connection = DataBaseManager.getConnection();
            assertNotNull(connection);
            assertFalse(connection.isClosed());
        } catch (TodoAppException | SQLException e) {
            fail("An error has occurred: " + e.getMessage());
        }
    }

    public static class App extends Application {

        public static void main(String[] args) {
            launch(args);
        }

        @Override
        public void start(Stage stage) throws IOException {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/fxml/Login-view.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 1200, 720);
            stage.setTitle("To-Do List - Login");
            stage.setScene(scene);
            stage.show();
        }
    }
}