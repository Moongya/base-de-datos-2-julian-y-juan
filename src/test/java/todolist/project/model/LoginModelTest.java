package todolist.project.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import todolist.project.exception.TodoAppException;
import java.sql.Connection;

import static org.junit.jupiter.api.Assertions.*;

public class LoginModelTest {

    private LoginModel loginModel;

    @BeforeEach
    public void setUp() throws TodoAppException {
        Connection connection = DataBaseManager.getConnection();
        loginModel = new LoginModel();
    }

    @Test
    public void testCheckInformationToLogin() {
        String validUsername = "testUser";
        String validPassword = "testPassword";

        try {
            assertTrue(loginModel.checkInformationToLogin(validUsername, validPassword));
            User user = loginModel.getUser();
            assertNotNull(user);
            assertEquals(validUsername, user.getUsername());
        } catch (TodoAppException e) {
            fail("A problem occurred when checking information to login");
        }
    }
}