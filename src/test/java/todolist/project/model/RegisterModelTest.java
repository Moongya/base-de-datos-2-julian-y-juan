package todolist.project.model;
import org.junit.jupiter.api.Test;
import todolist.project.exception.*;

import static org.junit.jupiter.api.Assertions.*;

public class RegisterModelTest {

    @Test
    public void testValidateEmpty_AllFieldsEmpty() {
        RegisterModel registerModel = new RegisterModel("", "", "", "", "");
        assertThrows(EmptyInputException.class, registerModel::validateEmpty);
    }

    @Test
    public void testValidateEmpty_SomeFieldsEmpty() {
        RegisterModel registerModel = new RegisterModel("", "Juan Torres", "juan@example.com", "password", "");
        assertThrows(EmptyInputException.class, registerModel::validateEmpty);
    }

    @Test
    public void testIsValidUsername_ValidUsername() {
        RegisterModel registerModel = new RegisterModel("Juanve003", "Juan Torres", "juan@example.com", "password", "password");
        assertDoesNotThrow(registerModel::isValidUsername);
    }

    @Test
    public void testIsValidUsername_InvalidUsernameWithSpace() {
        RegisterModel registerModel = new RegisterModel("Juanve003", "Juan Torres", "juan@example.com", "password", "password");
        assertThrows(InvalidInputForm.class, registerModel::isValidUsername);
    }

    @Test
    public void testIsValidEmail_ValidEmail() {
        RegisterModel registerModel = new RegisterModel("Juanve003", "Juan Torres", "juan@example.com", "password", "password");
        assertDoesNotThrow(registerModel::isValidEmail);
    }

    @Test
    public void testIsValidEmail_InvalidEmail() {
        RegisterModel registerModel = new RegisterModel("Juanve003", "Juan Torres", "juanexample.com", "password", "password");
        assertThrows(InvalidEmailException.class, registerModel::isValidEmail);
    }

}