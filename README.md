# Lista de Tareas

TO-DO List
Alpha Coders
Section C

Una aplicación simple para gestionar tus tareas diarias.


## Instalación

Sigue estos pasos para instalar y configurar la aplicación en tu entorno local:

1. **Requisitos previos:**
   - Asegúrate de tener instalado Java Development Kit (JDK), la versión 20 en tu sistema. Puedes descargarlo desde [aquí](https://www.oracle.com/java/technologies/downloads/).
   - Asegúrate de tener un servidor de base de datos MySQL en ejecución en tu máquina local. Puedes instalarlo siguiendo las instrucciones en [este enlace](https://dev.mysql.com/downloads/mysql/).

2. **Clona el repositorio:**
git clone https://gitlab.com/XimenaB08/base-de-datos-2.git

3. **Configuración de la base de datos:**
- Crea una nueva base de datos MySQL llamada `projectdb`.
- Ejecuta el script `dbs.sql` proporcionado en la siguiente ruta src/main/resources/database/dbs.sql

4. **Configuración del proyecto:**
- Abre el proyecto en tu IDE preferido (por ejemplo, IntelliJ IDEA o Eclipse).
- Actualiza la información que hay en la clase DataBaseManager dentro del paquete model, configura la url de tu base de datos, tu nombre de usuario (por defecto es root) y la contraseña

5. **Ejecuta la aplicación:**
- Ejecuta la clase principal `App` para iniciar la aplicación.